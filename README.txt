###################################
##           KEYBOARD SCANCODE                      ##
###################################

FREEWARE:
This program is a freeware which means that you may
copy it, distribute it by any means freely. The author
is not responsible for any damage caused by the use
of this program on any device. If however any one wants
to change the contents of the program, please contact
the author.

USAGE:
This program is quite simple to use. All one has to do
is to press the keys and get their scan code(RAW one)
that is recieved from keyboard port.


CONTENTS:
Readme.txt, Keyboard.bas, Keyboard.exe

CONTACT:
Developed by: Subhajit Sahu
Mail Address: wolfram77@gmail.com

**********************************



END