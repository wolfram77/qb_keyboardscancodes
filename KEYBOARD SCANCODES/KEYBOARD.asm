;--------------------------------------------------------------------------------
;		KEYBOARD SCANCODE
;--------------------------------------------------------------------------------
; Part of HZD Library
;
; Version: first
; by WolfRAM
;********************************************************************************





.MODEL Large, Basic

.386

INCLUDE	Includes.inc


;SHARED



.STACK 100h





;CONST
irq1					equ	9h*4
irqport					equ	20h
keyport					equ	60h
keytellport				equ	61h
extendedkey				equ	0E0h
keycurrentsize				equ	8
maxbuffersize				equ	256
keyfree1					equ	04
keyfree2					equ	06
keynumlocked				equ	2Ah
keyctrl					equ	1Dh
keyalt					equ	38h








.DATA
KeyActive					DB	0
OldKybdSEG				DW	0
OldKybdOFF				DW	0
KeyLastPress				DB	0
BreakKeySave				DB	1
KeyWasExtended				DB	0
KeyBuffer					DB	maxbuffersize DUP (0)
KeyCurrent				DB	keycurrentsize DUP (0)
KeyCurrentFlag				DB	16 DUP(0)
KeyBuuSize				DW	0
KeyBuuFront				DW	maxbuffersize-1
KeyBuuRear				DW	maxbuffersize-1







;External SUBs




.CODE










; ---------------------------------------------------------------------------
;		INTERNAL FUNCTIONS
; ---------------------------------------------------------------------------


; ---------------------------------------------------------------------------
; PushToKeyBuffer			INTERNAL FUNCTION
; Purpose:
;   Pushes data to the keyboard buffer
;
; Usage:
;   cl=data to push
;
; Returns:
;   bx=destroyed
; ---------------------------------------------------------------------------
EVEN
PUBLIC PushToKeyBuffer
PushToKeyBuffer PROC
cmp	KeyBuuSize, maxbuffersize
jae	keybufferfull
inc	KeyBuuSize
dec	KeyBuuRear

keybufferfull:
inc	KeyBuuRear
cmp	KeyBuuRear, maxbuffersize
jb	queuerearok
sub	KeyBuuRear, maxbuffersize

queuerearok:
inc	KeyBuuFront
cmp	KeyBuuFront, maxbuffersize
jb	queuefrontok
sub	KeyBuuFront, maxbuffersize

queuefrontok:
mov	bx, KeyBuuFront
mov	KeyBuffer[bx], cl
retf
PushToKeyBuffer ENDP




; ---------------------------------------------------------------------------
; StartKeyboard			INTERNAL FUNCTION
; Purpose:
;   Starts the keyboard
;
; Usage:
;   none
;
; Returns:
;   nothing
; ---------------------------------------------------------------------------
EVEN
PUBLIC StartKeyboard
StartKeyboard PROC
push2	es, ax
xor	ax, ax
mov	es, ax
mov	ax, es:[irq1+2]
mov	OldKybdSEG, ax
mov	ax, es:[irq1]
mov	OldKybdOFF, ax
mov	ax, SEG KeyboardISR
cli
mov	es:[irq1+2], ax
mov	ax, OFFSET KeyboardISR
mov	es:[irq1], ax
mov	KeyActive, 1
sti
pop2	es, ax
retf

KeyboardISR:
sti
push4	ds, ax, bx, cx
mov	ax, @DATA
mov	ds, ax
in	al, keyport
mov	cl, al
call	PushToKeyBuffer

keyhandleover:
in	al, keytellport
or	al, 80h
out	keytellport, al
mov	al, 20h
out	irqport, al
pop4	ds, ax, bx, cx
iret
StartKeyboard ENDP




; ---------------------------------------------------------------------------
; StopKeyboard			INTERNAL FUNCTION
; Purpose:
;   Stops the keyboard
;
; Usage:
;   none
;
; Returns:
;   nothing
; ---------------------------------------------------------------------------
EVEN
PUBLIC StopKeyboard
StopKeyboard PROC
push2	es, ax
xor	ax, ax
mov	es, ax
cli
mov	ax, OldKybdSEG
mov	es:[irq1+2], ax
mov	ax, OldKybdOFF
mov	es:[irq1], ax
mov	KeyActive,0
sti
mov	KeyBuuSize, 0
mov	KeyBuuFront, maxbuffersize-1
mov	KeyBuuRear, maxbuffersize-1
pop2	es, ax
retf
StopKeyboard ENDP










; ---------------------------------------------------------------------------
;		PUBLIC FUNCTIONS
; ---------------------------------------------------------------------------






; ---------------------------------------------------------------------------
; HZDkeyClear	SUB
;
; Purpose:
;   Clear the keyboard buffer
;
; Declaration:
;   DECLARE SUB HZDkeyClear()
;
; Returns:
;   nothing
; ---------------------------------------------------------------------------
EVEN
PUBLIC HZDkeyClear
HZDkeyClear PROC
cli
mov	KeyBuuSize, 0
mov	KeyBuuFront, maxbuffersize-1
mov	KeyBuuRear, maxbuffersize-1
sti
retf
HZDkeyClear ENDP




; ---------------------------------------------------------------------------
; HZDrawKeyPressed	FUNCTION
;
; Purpose:
;   Gives the oldest key that was pressed(in RAW form)
;
; Declaration:
;   DECLARE FUNCTION HZDrawKeyPressed%()
;
; Returns:
;   last key pressed
; ---------------------------------------------------------------------------
EVEN
PUBLIC HZDrawKeyPressed
HZDrawKeyPressed PROC
mov	ax, -1
cmp	KeyBuuSize, 0
jz	keypressover
cli
dec	KeyBuuSize
inc	KeyBuuRear
cmp	KeyBuuRear, maxbuffersize
jb	pressqrearok
sub	KeyBuuRear, maxbuffersize

pressqrearok:
mov	bx, KeyBuuRear
mov	al, KeyBuffer[bx]
sti
xor	ah, ah

keypressover:
retf
HZDrawKeyPressed ENDP




; ---------------------------------------------------------------------------
; HZDkeyPressed	FUNCTION
;
; Purpose:
;   Gives the oldest key that was pressed
;
; Declaration:
;   DECLARE FUNCTION HZDkeyPressed%()
;
; Returns:
;   last key pressed
; ---------------------------------------------------------------------------
EVEN
PUBLIC HZDkeyPressed
HZDkeyPressed PROC
call	HZDrawKeyPressed
retf
HZDkeyPressed ENDP





END